# Getting started with Terraform

[![video](video_preview.png)](https://youtu.be/EWLjK2gMfy4)

Terraform is an open-source tool from HashiCorp that is utilized for defining, maintaining, and provisioning infrastructure as code (IaC) in a declarative manner. 

This tutorial will guide you through installing Terraform on your system and showcase how you can provision infrastructure resources through Terraform. We will be deploying a local Nginx server through a Docker container and validate the container is up and running through our web browser. 


## Prerequisites

* [Terraform](https://www.terraform.io/downloads.html) version `0.13` or later
* [Docker](https://www.docker.com/products/docker-desktop)
* Terminal/Command Line
* Web Browser


## Install Terraform

To install Terraform, visit [Terraform.io](https://www.terraform.io/downloads.html) and download the compressed binary for your platform, machine or environment on which you like to develop and execute the configuration in.

Terraform comes as a single binary that can be executed in your environment without requiring additional dependencies. The file downloaded from [Terraform.io](https://www.terraform.io/downloads.html) is in a zip format and needs to be unzipped in order to access the Terraform binary. Once the zip file is unzipped we need to move the Terraform binary to our system's local `PATH`. If needed, here are instructions for setting [PATH](https://superuser.com/questions/284342/what-are-path-and-other-environment-variables-and-how-can-i-set-or-use-them) variables in Windows/Linux. 

### Linux/Unix/MacOS
Navigate to the location where the Terraform zip file was downloaded. Once you are in that location execute the commands below.

```bash
# Linux/Unix/MacOS
unzip terraform_0.14.7_linux_amd64
sudo mv terraform /usr/local/bin/
```

### Windows
Navigate to the location where the Terraform zip file was downloaded.
Extract the package to the folder `C:\Program Files (x86)`.
The `C:\Program Files (x86)` path is used as an example and should not be assumed to be in your `PATH` variable. However, you can also place the Terraform executable to any other location in your local system. Lastly, update the path environment variable to include the folder where your Terraform executable is located. Check out this [link](https://stackoverflow.com/questions/1618280/where-can-i-set-path-to-make-exe-on-windows) for how to update Windows path environment variables. 

**NOTE**: If your system (Windows/MacOS) does not have unzip pre-installed then feel free to use your system's file finder/viewer tool to locate the zip file and unzip through this native tool. The unzip action is conducted by right hand clicking on the zip file and selecting the desired location to where you want to extract the content to. Extract the Terraform binary to the location specified in the commands above depending on your operating system.

## Verify Installation

Verify Terraform was installed correctly by opening up a new terminal window and issuing the command `terraform version`

```shell
$ terraform version
Terraform v0.14.7
```

You should receive an output listing the current version of Terraform that is installed. 

**NOTE:** If you get an error stating that `terraform` could not be found,   please revisit the environment variables and ensure the location where the Terraform binary was placed is included in the `PATH` variable.


## Setting up the Terraform project

The next step is to define the project structure that will contain all our project files.

### Create project directory

Let us start by creating a new directory on our local system and navigating inside of it. Issue the two commands specified below.

```bash
$ mkdir terraform-demo
$ cd terraform-demo/
```
With the project folder created we will move onto the next step of creating a Terraform configuration file.

### Defining a provider
Start by creating a file that will contain our Terraform configuration.

```shell
# Linux/Unix/MacOS
$ touch main.tf
```
Paste the following content into the `main.tf` file.

```hcl
terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
    }
  }
}

provider "docker" {
    host = "unix:///var/run/docker.sock"
}
```
Our `main.tf` file now has a definition for all the providers we are using. In this tutorial we are using the `docker` provider. We also specified the address to where our docker host is located at `unix:///var/run/docker.sock`. To learn more about the Docker provider visit the Terraform Docker provider documentation [page](https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs).

### Writing Terraform Resources

Now that we have our Terraform providers defined in the `main.tf` file, the next step is to add the resources we want Terraform to create. Let us copy the lines below into our `main.tf` file.

```
# Find the latest nginx precise image.
resource "docker_image" "nginx-image" {
  name = "nginx:latest"
}

# Start a container
resource "docker_container" "running-container" {
  image = docker_image.nginx-image.latest
  name  = "training"
  ports {
    internal = 80
    external = 80
  }
}
```
These two resources work together. The first resource `docker_image` specifies what docker image we want to use, which in this case is the `nginx:latest`. The second resource `docker_container`, manages the life cycle of the container and starts our container named `training`, which contains the image we defined in our `docker_image` resource. We also expose the ports required to interact with this container.

At this point, we can go ahead and initialize our Terraform project.

### Terraform Init

Initialize Terraform with the `terraform init` command. 

```shell
 $ terraform init

Initializing the backend...

Initializing provider plugins...
- Finding latest version of kreuzwerker/docker...
- Installing kreuzwerker/docker v2.11.0...
- Installed kreuzwerker/docker v2.11.0 (self-signed, key ID 24E54F214569A8A5)

Partner and community providers are signed by their developers.
If you'd like to know more about provider signing, you can read about it here:
https://www.terraform.io/docs/cli/plugins/signing.html

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```
The `terraform init` command does a couple of things for us. First and foremost, it downloads all the required providers we defined earlier from the HashiCorp online registry. In our configuration this is the `kreuzwerker/docker` provider. The `terraform init` also generates a `terraform.lock.hcl` file. Think of this file as a tracker for the specific provider version we downloaded.

### Putting it all together
The `main.tf` file should have the following content (see below).
```hcl
terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
    }
  }
}


provider "docker" {
    host = "unix:///var/run/docker.sock"
}


# Find the latest nginx precise image.
resource "docker_image" "nginx-image" {
  name = "nginx:latest"
}

# Start a container
resource "docker_container" "running-container" {
  image = docker_image.nginx-image.latest
  name  = "training"
  ports {
    internal = 80
    external = 80
  }
}
```
## Terraform Plan (Preview Deploy)

Terraform comes with a built-in preview command that allows you to see what Terraform will do when the configuration we authored gets applied. The command is named `plan` and can be invoked by typing `terraform plan` in the command line/terminal and hitting the enter key.

Go ahead and type `terraform plan` in your command line/terminal and hit enter. You should be seeing an output like to what is presented below.

```shell
$ terraform plan

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # docker_container.running-container will be created
  + resource "docker_container" "running-container" {
      + attach           = false
      + bridge           = (known after apply)
      + command          = (known after apply)
      + container_logs   = (known after apply)
      + entrypoint       = (known after apply)
      + env              = (known after apply)
      + exit_code        = (known after apply)
      + gateway          = (known after apply)
      + hostname         = (known after apply)
      + id               = (known after apply)
      + image            = (known after apply)
      + init             = (known after apply)
      + ip_address       = (known after apply)
      + ip_prefix_length = (known after apply)
      + ipc_mode         = (known after apply)
      + log_driver       = "json-file"
      + logs             = false
      + must_run         = true
      + name             = "training"
      + network_data     = (known after apply)
      + read_only        = false
      + remove_volumes   = true
      + restart          = "no"
      + rm               = false
      + security_opts    = (known after apply)
      + shm_size         = (known after apply)
      + start            = true
      + stdin_open       = false
      + tty              = false

      + healthcheck {
          + interval     = (known after apply)
          + retries      = (known after apply)
          + start_period = (known after apply)
          + test         = (known after apply)
          + timeout      = (known after apply)
        }

      + labels {
          + label = (known after apply)
          + value = (known after apply)
        }

      + ports {
          + external = 80
          + internal = 80
          + ip       = "0.0.0.0"
          + protocol = "tcp"
        }
    }

  # docker_image.nginx-image will be created
  + resource "docker_image" "nginx-image" {
      + id     = (known after apply)
      + latest = (known after apply)
      + name   = "nginx:latest"
      + output = (known after apply)
    }

Plan: 2 to add, 0 to change, 0 to destroy.

------------------------------------------------------------------------

Note: You didn't specify an "-out" parameter to save this plan, so Terraform
can't guarantee that exactly these actions will be performed if
"terraform apply" is subsequently run.
```

If we review the output generated by `terraform plan`, we come to learn that `2` resources are being added, `0` resources are being changed, and that `0` resources are being destroyed. The output also includes `+` and `-` signs that helps us see more clearly what type of change it is.

A closer inspection of the output generated reveals what attributes our resources will have. Some of these attributes will be known after the apply, while other attributes will have the values we specified in the `main.tf` file. The `ports` attribute of the resource `resource "docker_container" "nginx"` is a great example of this. Notice how we specified port `80` in our configuration and that is the value the `terraform plan` output specified.

**NOTE:** If you are wondering where some of the values in the `terraform plan` output comes from know that these are default values for the respective resource. To learn more about a resource's default values visit the terraform [documentation](https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs) for the respective resource.


## Terraform Apply (Deploy)

We are now ready to provision the infrastructure we defined in our `main.tf` file. Provision the docker container by issuing the command `terraform apply`. Terraform will ask if you would like to continue, type `yes` and press enter to continue. The command may take a few minutes to run and will display a message indicating that the resource was created.


```shell
terraform apply

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # docker_container.running-container will be created
  + resource "docker_container" "running-container" {
      + attach           = false
      + bridge           = (known after apply)
      + command          = (known after apply)
      + container_logs   = (known after apply)
      + entrypoint       = (known after apply)
      + env              = (known after apply)
      + exit_code        = (known after apply)
      + gateway          = (known after apply)
      + hostname         = (known after apply)
      + id               = (known after apply)
      + image            = (known after apply)
      + init             = (known after apply)
      + ip_address       = (known after apply)
      + ip_prefix_length = (known after apply)
      + ipc_mode         = (known after apply)
      + log_driver       = "json-file"
      + logs             = false
      + must_run         = true
      + name             = "training"
      + network_data     = (known after apply)
      + read_only        = false
      + remove_volumes   = true
      + restart          = "no"
      + rm               = false
      + security_opts    = (known after apply)
      + shm_size         = (known after apply)
      + start            = true
      + stdin_open       = false
      + tty              = false

      + healthcheck {
          + interval     = (known after apply)
          + retries      = (known after apply)
          + start_period = (known after apply)
          + test         = (known after apply)
          + timeout      = (known after apply)
        }

      + labels {
          + label = (known after apply)
          + value = (known after apply)
        }

      + ports {
          + external = 80
          + internal = 80
          + ip       = "0.0.0.0"
          + protocol = "tcp"
        }
    }

  # docker_image.nginx-image will be created
  + resource "docker_image" "nginx-image" {
      + id     = (known after apply)
      + latest = (known after apply)
      + name   = "nginx:latest"
      + output = (known after apply)
    }

Plan: 2 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

docker_image.nginx-image: Creating...
docker_image.nginx-image: Still creating... [10s elapsed]
docker_image.nginx-image: Creation complete after 14s [id=sha256:35c43ace9216212c0f0e546a65eec93fa9fc8e96b25880ee222b7ed2ca1d2151nginx:latest]
docker_container.running-container: Creating...
docker_container.running-container: Creation complete after 2s [id=5b34d9074dc743095e1c1bdd9c849fa77fc2240eb8632403efe8f94ad5c7206c]

Apply complete! Resources: 2 added, 0 changed, 0 destroyed.
```

## Validate nginx container

To confirm that our `nginx` container is up and running open your web browser and visit the url `localhost:80`. If everything was deployed successfully you will see a welcome message.

```
Welcome to nginx!
If you see this page, the nginx web server is successfully installed and working. Further configuration is required.

For online documentation and support please refer to nginx.org.
Commercial support is available at nginx.com.

Thank you for using nginx.
```


## Terraform Destroy (Clean-up)

It is time to remove the resources we deployed. To remove the resources we defined in the `main.tf` file we will execute the command `terraform destroy`. This command should be exercised with caution. Terraform will again prompt you if you would like to continue with this action. Type `yes` and press the enter key to continue.

```shell
$ terraform destroy

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  - destroy

Terraform will perform the following actions:

  # docker_container.running-container will be destroyed
  - resource "docker_container" "running-container" {
      - attach            = false -> null
      - command           = [
          - "nginx",
          - "-g",
          - "daemon off;",
        ] -> null
      - cpu_shares        = 0 -> null
      - entrypoint        = [
          - "/docker-entrypoint.sh",
        ] -> null
      - env               = [] -> null
      - gateway           = "172.17.0.1" -> null
      - hostname          = "5b34d9074dc7" -> null
      - id                = "5b34d9074dc743095e1c1bdd9c849fa77fc2240eb8632403efe8f94ad5c7206c" -> null
      - image             = "sha256:35c43ace9216212c0f0e546a65eec93fa9fc8e96b25880ee222b7ed2ca1d2151" -> null
      - init              = false -> null
      - ip_address        = "172.17.0.2" -> null
      - ip_prefix_length  = 16 -> null
      - ipc_mode          = "private" -> null
      - log_driver        = "json-file" -> null
      - logs              = false -> null
      - max_retry_count   = 0 -> null
      - memory            = 0 -> null
      - memory_swap       = 0 -> null
      - must_run          = true -> null
      - name              = "training" -> null
      - network_data      = [
          - {
              - gateway                   = "172.17.0.1"
              - global_ipv6_address       = ""
              - global_ipv6_prefix_length = 0
              - ip_address                = "172.17.0.2"
              - ip_prefix_length          = 16
              - ipv6_gateway              = ""
              - network_name              = "bridge"
            },
        ] -> null
      - network_mode      = "default" -> null
      - privileged        = false -> null
      - publish_all_ports = false -> null
      - read_only         = false -> null
      - remove_volumes    = true -> null
      - restart           = "no" -> null
      - rm                = false -> null
      - security_opts     = [] -> null
      - shm_size          = 64 -> null
      - start             = true -> null
      - stdin_open        = false -> null
      - tty               = false -> null

      - ports {
          - external = 80 -> null
          - internal = 80 -> null
          - ip       = "0.0.0.0" -> null
          - protocol = "tcp" -> null
        }
    }

  # docker_image.nginx-image will be destroyed
  - resource "docker_image" "nginx-image" {
      - id     = "sha256:35c43ace9216212c0f0e546a65eec93fa9fc8e96b25880ee222b7ed2ca1d2151nginx:latest" -> null
      - latest = "sha256:35c43ace9216212c0f0e546a65eec93fa9fc8e96b25880ee222b7ed2ca1d2151" -> null
      - name   = "nginx:latest" -> null
    }

Plan: 0 to add, 0 to change, 2 to destroy.

Do you really want to destroy all resources?
  Terraform will destroy all your managed infrastructure, as shown above.
  There is no undo. Only 'yes' will be accepted to confirm.

  Enter a value: yes

docker_container.running-container: Destroying... [id=5b34d9074dc743095e1c1bdd9c849fa77fc2240eb8632403efe8f94ad5c7206c]
docker_container.running-container: Destruction complete after 2s
docker_image.nginx-image: Destroying... [id=sha256:35c43ace9216212c0f0e546a65eec93fa9fc8e96b25880ee222b7ed2ca1d2151nginx:latest]
docker_image.nginx-image: Destruction complete after 1s

Destroy complete! Resources: 2 destroyed.
```
All resources are now destroyed, and we have successfully cleaned up after ourselves.

## Next Steps

Let us review what we have accomplished in this tutorial. Our goal was to deploy an `nginx` container and use our browser to confirm that the container was up and running. We installed Terraform on our system and we created a project folder named `terraform-demo`. 

We defined the desired infrastructure resources we wanted to deploy by using Terraform and authoring a configuration file using Terraform's declarative language. We then deployed the resources by using the command `terraform deploy` and we were able to confirm our `nginx` container was deployed successfully by visiting `localhost:80` in our web browser. Lastly, we cleaned up after ourselves by destroying all of our resources through the `terraform destroy` command.

Terraform provides many benefits as you have now experienced through this tutorial.  From defining infrastructure in a human readable form, to managing the full life cylce of infrastructure resources (create, manage existing, and destroy). Most importantly, Terraform allows us to create reproducible infrastructure. 

Please visit https://learn.hashicorp.com/terraform to learn more about Terraform and how Terraform can improve infrastructure management in your projects.